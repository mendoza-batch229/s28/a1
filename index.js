#3
db.SingleR oom.insertOne({
    name: "Single",
    accomodates: 2,
    price: 1000,
    description: "A simple woom with all the basic necessities",
    rooms_available: 10,
    isAvailable: false
    })

 #4
db.MultipleRoom.insertMany([
    {
        name: "double",
        accomodation: 3,
        price: 2000,
        description: "A room fit for a small family",
        roome_available: 5,
        isAvailable: false
        },
    {
        name: "queen",
        accomodation: 4,
        price: 4000,
        description: "A room fit for a big family",
        roome_available: 15,
        isAvailable: false
        }
])

#5
db.MultipleRoom.find({name:"double"})

#6
db.MultipleRoom.updateOne({room_available: 15},{$set:{room_available: 0}})

#7
db.MultipleRoom.deleteMany({room_available: 0})
